//{
//  "page_id": "8762",
//  "page_type": "merchandise",
//  "merchandise_lang_agnostic_id": "5005004",
//  "unit_price_in_mill": "30000",
//  "quantity": "1",
//  "variants": "null",
//  "note": null,
//  "default_image_name": null,
//  "status": "1"
//}
var totalprice = 0;
function get_totalprice(num,price){		
	totalprice += (parseInt(num)*parseInt(price));
	$(".cartli2 span").html("¥"+(totalprice/1000.0));	
//	console.log(totalprice);
//	return totalprice;	
};



//接口对接部分 END

//头部中英文切换按钮
var cartcon = document.getElementById("cartcon");
var ordercon = document.getElementById("ordercon");
var messagecon = document.getElementById("messagecon");
var languagecon = document.getElementById("languagecon");
//var languagep = language.getElementsByTagName("p");
var languagep = languagecon.getElementsByTagName("p");
var carticon = document.getElementsByClassName("carticon");
var ordericon = document.getElementsByClassName("ordericon");
var messageicon = document.getElementsByClassName("messageicon");
var delmgs = document.getElementsByClassName("delmg");
//shoppcart事件
var cartcon = document.getElementById("cartcon");
var confirmorder = document.getElementById("confirmorder");
var paymentmethod = document.getElementById("paymentmethod");
var confirmbtn0 = document.getElementById("confirmbtn0");
var confirmbtn1 = document.getElementById("confirmbtn1");
var return1 = document.getElementById("return1");
var return2 = document.getElementById("return2");
var return3 = document.getElementsByClassName("return3");
var return6 = document.getElementById("return6");
var return7 = document.getElementById("return7");
var additional = document.getElementById("additional");
var additionalto = document.getElementById("additionalto");
var payment = document.getElementById("payment");
var paydivs = payment.getElementsByTagName("div");
var orderpayment = document.getElementById("orderpayment");

//var orderpaylis = orderpayment.getElementsByTagName("li");
var alipy = document.getElementsByClassName("alipy");
var deletes = document.getElementsByClassName("delete");
//编辑icon
var writes = document.getElementsByClassName("write");
var writenow = document.getElementsByClassName("writenow");
var txtears = document.getElementsByClassName("txtear");
var writecopy = document.getElementsByClassName("writecopy");
var submitsub = document.getElementsByClassName("submitsub");
var orderpaymentmethod = document.getElementById("orderpaymentmethod");
var orderalipy = document.getElementById("orderalipy");
//orderList tab栏
var ordertitle = document.getElementById("ordertitle");
var ordercon = document.getElementById("ordercon");
var spans = ordertitle.getElementsByTagName("span");
var uls = ordercon.getElementsByTagName("ul");
var li0 = uls[0].getElementsByTagName("img");
var li1 = uls[1].getElementsByTagName("img");
var li2 = uls[2].getElementsByTagName("img");
var orderdetail = document.getElementById("orderdetail");
var paynow = document.getElementById("paynow");
var return0 = document.getElementById("return0");
var ordericon = document.getElementsByClassName("ordericon");
//导航条上商品的增删改查
var cartminus = document.getElementsByClassName("cartminus");
var cartadd = document.getElementsByClassName("cartadd");
var cartnum = document.getElementsByClassName("cartnum");
var cartli2 = document.getElementsByClassName("cartli2");
var totalmoney = cartli2[0].getElementsByTagName("span");
var liprice = document.getElementsByClassName("liprice");
//点击购物车icon时 
$("#cart")[0].ontouchend = function(e){
	e.preventDefault();
	
	//接口对接部分
	var default_language = "zh-cn";
	var url = API_URL + "cart.get_info&room_id="+ROOMID;
	var data_end = $.when(get_data(url)).done(function(json){
	//	console.log(json);
		var data = json.data;
		$(".prolist").empty();
		$.each(data, function(i) {
			var str = '';
			
			$.when(get_data(API_URL + "item.get_info&id="+data[i].page_id+"&page_type="+data[i].page_type))
			.done(function(json){
	//			 	判断是否有图片
				var merchandise_default_image = (json.data.default_image !=null) ? json.data.default_image.internal_image_url : '';
				
				str += '<li class="cartli0">';
				str += '	<a href="detail.html?id='+json.data.id+'&type='+json.data.page_type+'"><img src="'+merchandise_default_image+'"/></a>';
				str += '	<div id="">';
				str += '		<p class="ordertitle">'+json.data.translates[default_language].title+'<span class="liprice" >¥'+handlePrice(json.data.default_price_in_mill)+'</span></p>';
				str += '		<p class="orderdetail">Small / Black pepper / Mushroom</p>';
				str += '		<p class="writenow"><textarea name="" rows="" cols="" class="txtear"></textarea><button class="submitsub">Submit</button></p>';
				str += '		<p class="writecopy cur">&nbsp;</p>';
				str += '	</div>	';
				str += '	<img src="images/delete.png" class="delete" onclick="delete_shangpin(this);"/>';
				str += '	<img src="images/write.png" class="write"/>';
				str += '	<img src="images/minus.png" class="cartminus" onclick="minus_num(this);"/>';
				str += '	<span class="cartnum">'+data[i].quantity+'</span>';
				str += '	<img src="images/add.png" class="cartadd" onclick="add_num(this);"/>	';
				str += '</li>';
				$(".prolist").append(str);
				
	//			get_totalprice(json.data.default_price_in_mill,data[i].quantity);
		
				
			});
	//		console.log(str);
		});
	
	});
	data_end.done(function(json){
	
		$.each(json.data, function(i) {		
			get_totalprice(json.data[i].unit_price_in_mill,json.data[i].quantity);
		});
		
	});
//	为其他icon初始化
	ordercon.className = "";
	ordericon[0].className = "ordericon";
	messagecon.className = "";
	messageicon[0].className = "messageicon";
	languagecon.className = "";
	$(".orderdetails").removeClass("cur");
//	支付方式和详情
	$("#paymentmethod").removeClass("cur");	
	$("#alipy").removeClass("cur");
	$("#wechat").removeClass("cur");
	$("#coupons").removeClass("cur");
	$("#cash").removeClass("cur");
	$("#creditcard").removeClass("cur");
	$("#roomfolio").removeClass("cur");
//	初始化结束
	if(cartcon.className == "cur"){		
		cartcon.className = "";
		carticon[0].className = "carticon";	
	}else if(confirmorder.className == "cur"){
		confirmorder.className = "";
		carticon[0].className = "carticon";
	}else if($(".carticon").hasClass("cur")){
		paymentmethod.className = "";
		carticon[0].className = "carticon";
	}
	else if($("#alipy").hasClass("cur")){
		$("#alipy").removeClass("cur");
		carticon[0].className = "carticon";
	}else if($("#wechat").hasClass("cur")){
		$("#wechat").removeClass("cur");
		carticon[0].className = "carticon";
	}else if($("#coupons").hasClass("cur")){
		$("#coupons").removeClass("cur");
		carticon[0].className = "carticon";
	}else if($("#cash").hasClass("cur")){
		$("#cash").removeClass("cur");
		carticon[0].className = "carticon";
	}else if($("#creditcard").hasClass("cur")){
		$("#creditcard").removeClass("cur");
		carticon[0].className = "carticon";
	}else if($("#roomfolio").hasClass("cur")){
		$("#roomfolio").removeClass("cur");
		carticon[0].className = "carticon";
	}
	else{
		cartcon.className = "cur";
		carticon[0].className = "carticon cur";
	}
}
//叫服务
$("#order")[0].ontouchend = function(e){
	e.preventDefault();
//	为其他icon初始化
	cartcon.className = "";
	carticon[0].className = "carticon";	
	messagecon.className = "";
	messageicon[0].className = "messageicon";
	languagecon.className = "";
//	初始化结束
	if(ordercon.className == "cur"){		
		ordercon.className = "";
		ordericon[0].className = "ordericon";
	}else if($(".orderdetails").hasClass("cur")){
		$(".orderdetails").removeClass("cur");
		ordericon[0].className = "ordericon";
	}
	else if($("#paymentmethodorder").hasClass("cur")){
		$("#paymentmethodorder").removeClass("cur");
		ordericon[0].className = "ordericon";
	}
	else if($("#alipyorder").hasClass("cur")){
		$("#alipyorder").removeClass("cur");
		ordericon[0].className = "ordericon";
	}else if($("#wechatorder").hasClass("cur")){
		$("#wechatorder").removeClass("cur");
		ordericon[0].className = "ordericon";
	}else if($("#couponsorder").hasClass("cur")){
		$("#couponsorder").removeClass("cur");
		ordericon[0].className = "ordericon";
	}else if($("#cashorder").hasClass("cur")){
		$("#cashorder").removeClass("cur");
		ordericon[0].className = "ordericon";
	}else if($("#creditcardorder").hasClass("cur")){
		$("#creditcardorder").removeClass("cur");
		ordericon[0].className = "ordericon";
	}else if($("#roomfolioorder").hasClass("cur")){
		$("#roomfolioorder").removeClass("cur");
		ordericon[0].className = "ordericon";
	}
	else{
		ordercon.className = "cur";
		ordericon[0].className = "ordericon cur";
	}
}
//信息推送
$("#message")[0].ontouchend = function(e){
	e.preventDefault();
//	为其他icon初始化
	cartcon.className = "";
	carticon[0].className = "carticon";	
	ordercon.className = "";
	ordericon[0].className = "ordericon";
	languagecon.className = "";
//	初始化结束
	if(messagecon.className == "cur"){		
		messagecon.className = "";
		messageicon[0].className = "messageicon";
	}else{
		messagecon.className = "cur";
		messageicon[0].className = "messageicon cur";
	}
}
//消息删除按钮
$.each(delmgs,function(j){
	delmgs[j].ontouchend = function(e){
		e.preventDefault();
		this.parentNode.parentNode.removeChild(this.parentNode);
	}
})
//语言切换
$("#language")[0].ontouchend = function(e){
	e.preventDefault();
//	为其他icon初始化
	cartcon.className = "";
	carticon[0].className = "carticon";	
	ordercon.className = "";
	ordericon[0].className = "ordericon";
	messagecon.className = "";
	messageicon[0].className = "messageicon";
//	初始化结束
	if(languagecon.className == "cur"){		
		languagecon.className = "";
	}else{
		languagecon.className = "cur";
	}
}
//语言切换按钮背景色的改变
for(var i = 0;i < languagep.length; i++){
	languagep[i].index =i;
	languagep[i].ontouchend = function(e){
		e.preventDefault();
		for(var j = 0 ; j < languagep.length;j++){
			languagep[j].className = '';
		}
		this.className = 'on';
	}
}
//myorder tab
for(var m = 0;m < spans.length; m++){
	spans[m].index = m;
	spans[m].ontouchend = function(e){
		e.preventDefault();
		for(var n = 0 ; n < spans.length; n++){
			spans[n].className = '';
		}
		this.className = 'cur';
		var li_index = this.index;
		var div_index = li_index;
		for(var n = 0; n < uls.length;n++){
			uls[n].className = '';			
		}
		uls[div_index].className = 'cur';
	}
}
//点击orderList中的某个跳到orderdetail
for(var i = 0; i < $(".sellerimg img").length; i++){
	$(".sellerimg img")[i].ontouchend = function(e){
		e.preventDefault();
		$("#ordercon").removeClass("cur");
		$(".orderdetails").addClass("cur");
	}
}
$(".return_orderlist")[0].ontouchend = function(e){
	e.preventDefault();
	$(".orderdetails").removeClass("cur");
	$("#ordercon").addClass("cur");
}
//点击orderdetails上的cancle order关闭弹框
$(".cancle_order")[0].ontouchend = function(e){
	e.preventDefault();
	$(".orderdetails").removeClass("cur");
	$(".ordericon").removeClass("cur");
}
//点击orderdetails上的pay now按钮
$(".paynow_btn")[0].ontouchend = function(e){
	e.preventDefault();
	$(".orderdetails").removeClass("cur");
	$("#paymentmethod").addClass("cur");
}
//for(var i = 0; i < li0.length;i++){
//	li0[i].ontouchend = function(e){
//		e.preventDefault();
//		ordercon.className = "";
//		orderdetail.className = "cur";
//	}
//}
//for(var j = 0; j < li0.length;j++){
//	li1[j].ontouchend = function(e){
//		e.preventDefault();
//		ordercon.className = "";
//		orderdetail.className = "cur";
//	}
//}
//for(var k = 0; k < li0.length;k++){
//	li2[k].ontouchend = function(e){
//		e.preventDefault();
//		ordercon.className = "";
//		orderdetail.className = "cur";
//	}
//}
//点击paynow事件
//paynow.ontouchend = function(e){
//	e.preventDefault();
//	orderdetail.className = "";
//	orderpaymentmethod.className = "cur";
//}
//点击return事件
//return0.ontouchend = function(e){
//	e.preventDefault();
//	ordercon.className = "cur";
//	orderdetail.className = "";
//}
//点击return6事件
//return6.ontouchend = function(e){
//	e.preventDefault();
//	orderpaymentmethod.className = "";
//	orderdetail.className = "cur";
//}
//点击return7事件
//return7.ontouchend = function(e){
//	e.preventDefault();
//	orderpaymentmethod.className = "cur";
//	orderalipy.className = "";
//}

//点击confirmbtn0事件
confirmbtn0.ontouchend = function(e){
	e.preventDefault();
	confirmorder.className = "cur";
	cartcon.className = "";
//	additionalto.innerHTML = additional.value;
}
//点击return1事件
return1.ontouchend = function(e){
	e.preventDefault();
	cartcon.className = "cur";
	confirmorder.className = "";
	additional.value = '';
}
//点击confirmbtn1事件
confirmbtn1.ontouchend = function(e){
	e.preventDefault();
	paymentmethod.className = "cur";
	confirmorder.className = "";
}
//点击return2事件
return2.ontouchend = function(e){
	e.preventDefault();
	confirmorder.className = "cur";
	paymentmethod.className = "";
}
//点击return3事件
return3.ontouchend = function(e){
	e.preventDefault();
	paymentmethod.className = "cur";
	alipy.className = "";
}
// 选择付款方式的排他
for(var p = 0;p < paydivs.length; p++){
	paydivs[p].index = p;
	paydivs[p].ontouchend = function(e){
		e.preventDefault();
		for(var q = 0 ; q < paydivs.length; q++){
			paydivs[q].className = '';
		}
		this.className = 'cur';
	}
}
//支付方式选择功能和支付页面回退功能
$(".alipydiv")[0].ontouchend = function(e){
	e.preventDefault();
	$("#paymentmethod").removeClass("cur");
	$("#alipy").addClass("cur");	
}
$(".return3")[0].ontouchend = function(e){
	e.preventDefault();
	$("#paymentmethod").addClass("cur");
	$("#alipy").removeClass("cur");
}
$(".wechatdiv")[0].ontouchend = function(e){
	e.preventDefault();
	$("#paymentmethod").removeClass("cur");
	$("#wechat").addClass("cur");	
}
$(".return3")[1].ontouchend = function(e){
	e.preventDefault();
	$("#paymentmethod").addClass("cur");
	$("#wechat").removeClass("cur");
}
$(".couponsdiv")[0].ontouchend = function(e){
	e.preventDefault();
	$("#paymentmethod").removeClass("cur");
	$("#coupons").addClass("cur");	
}
$(".return3")[2].ontouchend = function(e){
	e.preventDefault();
	$("#paymentmethod").addClass("cur");
	$("#coupons").removeClass("cur");
}
$(".cashdiv")[0].ontouchend = function(e){
	e.preventDefault();
	$("#paymentmethod").removeClass("cur");
	$("#cash").addClass("cur");	
}
$(".return3")[3].ontouchend = function(e){
	e.preventDefault();
	$("#paymentmethod").addClass("cur");
	$("#cash").removeClass("cur");
}
$(".creditcarddiv")[0].ontouchend = function(e){
	e.preventDefault();
	$("#paymentmethod").removeClass("cur");
	$("#creditcard").addClass("cur");	
}
$(".return3")[4].ontouchend = function(e){
	e.preventDefault();
	$("#paymentmethod").addClass("cur");
	$("#creditcard").removeClass("cur");
}
$(".roomfoliodiv")[0].ontouchend = function(e){
	e.preventDefault();
	$("#paymentmethod").removeClass("cur");
	$("#roomfolio").addClass("cur");	
}
$(".return3")[5].ontouchend = function(e){
	e.preventDefault();
	$("#paymentmethod").addClass("cur");
	$("#roomfolio").removeClass("cur");
}

// order选择付款方式的排他
//for(var p = 0;p < orderpaylis.length; p++){
//	orderpaylis[p].index = p;
//	orderpaylis[p].ontouchend = function(e){
//		e.preventDefault();
//		for(var q = 0 ; q < orderpaylis.length; q++){
//			orderpaylis[q].className = '';
//		}
//		this.className = 'cur';
//		orderalipy.className = "cur";
//		orderpaymentmethod.className = "";
//	}
//}


//shoppingcart商品增删改查
//商品加一按钮
function add_num(data){
	var num = parseInt($(data).parent().children(".cartnum").text());
	num ++;
	$(data).parent().children(".cartnum").text(num);
//	get_totalprice();
}
//商品减一按钮
function minus_num(data){
	var num = parseInt($(data).parent().children(".cartnum").text());
	var price = $(data).parent().find(".liprice2").text();
	if(num > 1){
		num --;
	}
	$(data).parent().children(".cartnum").text(num);
//	console.log(price.replace("¥",""));
//	get_totalprice(num,price);
//	get_totalprice();
}

//商品总价累加函数
//function get_totalprice(){
//	var splittotal = 0;
////	var total_price = 0;
//	for(var i = 0; i < $(".cartli0").length; i++){		
//		splittotal += parseInt(liprice[i].innerHTML.split("$")[1])*parseInt(cartnum[i].innerHTML);		
//	}
//	totalmoney[0].innerHTML = "$" + splittotal;
//}
//get_totalprice();
//调用累加函数


//点击购物车删除按钮事件
var delete_shangpin = function(data){
	data.parentNode.parentNode.removeChild(data.parentNode);
}

//点击编辑按钮事件
//function write_beizhu(data){
//	
//}
$.each(writes, function(i) {
	writes[i].ontouchend = function(e){
		e.preventDefault();
		if(writenow[i].className == "writenow cur"){
			writenow[i].className = "writenow";
			writecopy[i].className = "writecopy cur";
		}else{
			writenow[i].className = "writenow cur";
			writecopy[i].className = "writecopy";
		}		
	}
});
//点击submit按钮事件
$.each(submitsub, function(j) {
	submitsub[j].ontouchend = function(e){
		e.preventDefault();
		if(txtears[j].value == ''){
			writenow[j].className = "writenow";
			writecopy[j].className = "writecopy cur";
			writecopy[j].innerHTML = "&nbsp;";
		}else{
			writenow[j].className = "writenow";
			writecopy[j].className = "writecopy cur";
			writecopy[j].innerHTML = txtears[j].value;
		}	
	}	
});

