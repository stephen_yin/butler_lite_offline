    /**
     * 公共变量
     */
    var APISERVER = "http://panora.api98.com/";
    var PROJECT = "butler";
    var API_URL = APISERVER + PROJECT + "/?s=";
    var DEBUG = 1;
    

/**
 * GET请求数据方法 封装的AJAX，适用于Butler,ISTV，门锁等APP.
 * @param {string} Url
 * @param {string} KeyName
 * @returns {unresolved}
 */
function get_data(Url, KeyName) {
    var defer = $.Deferred();
    $.ajax({
        type: "get",
        url: Url,
        dataType: "json",
        
    }).then(function (json) {
        if (json['ret'] === 200) {
            defer.resolve(json);
            if (KeyName)
                localStorage.setItem(KeyName, JSON.stringify(json));
        } else {
            defer.reject(json);
            if(KeyName) 
                localStorage.removeItem(KeyName);
        }
    });
    return defer.promise();
}

/**
 * POST请求数据方法 封装的AJAX，适用于Butler,ISTV，门锁等APP.
 * @param {json} Datas
 * @param {string} Url
 * @param {string} KeyName
 * @returns {unresolved}
 */
function post_data(Datas, Url, KeyName) {
    var defer = $.Deferred();
    $.ajax({
        type: "post",
        url: Url,
        data: datas,
        dataType: "json",
        contentType: "application/x-www-form-urlencoded",
        
    }).then(function (json) {
        defer.resolve(json['data']);
        if (json['ret'] === 200) {
            defer.resolve(json);
            if (KeyName)
                localStorage.setItem(KeyName, JSON.stringify(json));
        } else {
            defer.reject(json);
            if(KeyName) 
                localStorage.removeItem(KeyName);
        }
    });
    return defer.promise();
}
