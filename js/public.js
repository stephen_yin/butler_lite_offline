//头部中英文切换按钮
var ORDERID = 0;
var cartcon = document.getElementById("cartcon");
var ordercon = document.getElementById("ordercon");
var messagecon = document.getElementById("messagecon");
var languagecon = document.getElementById("languagecon");
//var languagep = language.getElementsByTagName("p");
var languagep = languagecon.getElementsByTagName("p");
var carticon = document.getElementsByClassName("carticon");
var ordericon = document.getElementsByClassName("ordericon");
var messageicon = document.getElementsByClassName("messageicon");
var delmgs = document.getElementsByClassName("delmg");
//shoppcart事件
var cartcon = document.getElementById("cartcon");
var confirmorder = document.getElementById("confirmorder");
var paymentmethod = document.getElementById("paymentmethod");
var confirmbtn0 = document.getElementById("confirmbtn0");
var confirmbtn1 = document.getElementById("confirmbtn1");
var return1 = document.getElementById("return1");
var return2 = document.getElementById("return2");
var return3 = document.getElementsByClassName("return3");
var return6 = document.getElementById("return6");
var return7 = document.getElementById("return7");
var additional = document.getElementById("additional");
var additionalto = document.getElementById("additionalto");
var payment = document.getElementById("payment");
var paydivs = payment.getElementsByTagName("div");
var orderpayment = document.getElementById("orderpayment");
var alipy = document.getElementsByClassName("alipy");
var deletes = document.getElementsByClassName("delete");
//编辑icon
var writes = document.getElementsByClassName("write");
var writenow = document.getElementsByClassName("writenow");
var txtears = document.getElementsByClassName("txtear");
var writecopy = document.getElementsByClassName("writecopy");
var submitsub = document.getElementsByClassName("submitsub");
var orderpaymentmethod = document.getElementById("orderpaymentmethod");
var orderalipy = document.getElementById("orderalipy");
//orderList tab栏
var ordertitle = document.getElementById("ordertitle");
var ordercon = document.getElementById("ordercon");
var spans = ordertitle.getElementsByTagName("span");
var uls = ordercon.getElementsByTagName("ul");
var li0 = uls[0].getElementsByTagName("img");
var li1 = uls[1].getElementsByTagName("img");
var li2 = uls[2].getElementsByTagName("img");
var orderdetail = document.getElementById("orderdetail");
var paynow = document.getElementById("paynow");
var return0 = document.getElementById("return0");
var ordericon = document.getElementsByClassName("ordericon");
//导航条上商品的增删改查
var cartminus = document.getElementsByClassName("cartminus");
var cartadd = document.getElementsByClassName("cartadd");
var cartnum = document.getElementsByClassName("cartnum");
var cartli2 = document.getElementsByClassName("cartli2");
var totalmoney = cartli2[0].getElementsByTagName("span");
var liprice = document.getElementsByClassName("liprice");

//对购物车上数字进行同步


if(OFFLINE){
	localforage.getItem("cartsync", function (err, json) { 				
		
			if (err == null && json !== null){ 
				console.log("cartsync_read_success:",json);
				
				$("#carticonnum").html(json.data.length);
				if (parseInt(json.data.length) > 0) {
					$("#carticonnum").removeClass("cur");
				} else {
					$("#carticonnum").addClass("cur");
				}
				
				
			}else{
				console.log("cartsync_read_err:",err);
			}
			
			});
	
	
}else{
	
	var urlcart = API_URL + "cart.get_info&device_type=tablet&device_id=" + device_id + "&lang=" + DefaultLanguage;
	get_data(urlcart).then(function (json) {
		
			/** 缓存数据**/
		
					localforage.setItem("cartsync", json).then(function (jsonobj) {
						// 如下输出save_cartinfo
						console.log("save_cartsync:", jsonobj);
					}).catch(function (err) {
						// 当出错时，此处代码运行
						console.log(err);
					});
		
		
		$("#carticonnum").html(json.data.length);
		if (parseInt(json.data.length) > 0) {
			$("#carticonnum").removeClass("cur");
		} else {
			$("#carticonnum").addClass("cur");
		}
	})

}
//强制保留2位小数
function toDecimal2(x) {
	var f = parseFloat(x);
	if (isNaN(f)) return false;
	var f = Math.round(x * 100) / 100 / 1000;
	var s = f.toString();
	var rs = s.indexOf('.');
	if (rs < 0) {
		rs = s.length;
		s += '.';
	}
	while (s.length <= rs + 2) {
		s += '0';
	}
	return s;
}
//字符串转金额
function StrToMoney(str) {
	var arr = str.split("");
	var hasPoint = false;

	var j = 0;

	for (var i = 0; i < arr.length; i++) {
		if (arr[i] == ".") {
			hasPoint = true;
			continue;
		}

		if (hasPoint)
			j++;
	}

	if (j == 0) str += ".";
	for (; j < 2; j++) {
		str += "0";
	}
	return "$" + str;
}
//关闭蒙版函数
function cancelmengban(targ) {
	$(targ).parent().removeClass("cur");
	$(".carticon").removeClass("cur");
	$(".ordericon").removeClass("cur");
	$(".messageicon").removeClass("cur");
}

//对接接口部分
var list = JSON.parse(localStorage.getItem("list2"));
var json = JSON.parse(localStorage.getItem("butlerInfolist"));
//原始服务费
var surchargePermillage = JSON.parse(localStorage.getItem("butler")).surchargePermillage;
//计算服务费
var surchargenum = parseFloat(surchargePermillage) / 1000.0;
var DefaultLanguage = localStorage.getItem("DefaultLanguage");
$("#languagecon").find("p[data-lang='" + DefaultLanguage + "']").addClass("on").siblings().removeClass("on");

function langchange(trag) {
	$(trag).addClass("on").siblings().removeClass("on");
	window.localStorage.setItem("DefaultLanguage", $(trag).data("lang"));
	location.reload();
}
var imglist = JSON.parse(localStorage.getItem("imglist"));

function cartinfo() {
	if (OFFLINE) {

		localforage.getItem("cartinfo", function (err, json) { 				
			
			if (err == null && json !== null){ 
				
				$(".prolist").empty();
							$.each(json.data, function (i) {
								var defaultImageid = json.data[i].defaultImage;
								var defaultImage = ((defaultImageid !== null) && imglist.data[defaultImageid]) ? imglist.data[defaultImageid].internal_image_url :
									imglist.data[1484].internal_image_url;
								var str = '';
								var variants_obj = JSON.parse(json.data[i].variants);
								str += '<li class="cartli0">';
								str += '<a href="detail.html?id=' + json.data[i].page_id + '&type=MERCHANDISE"><img src="' + defaultImage +
									'"/></a>';
								str += '<div id="">';
								str += '<p class="ordertitle">' + json.data[i].translates[DefaultLanguage].title + '<span class="liprice" >$' +
									parseFloat(json.data[i].price_in_mill) / 1000.0 + '</span></p>';
								str += '<p class="orderdetail">';
								//			从接口中获取商品附加属性并显示
								if (variants_obj !== null) {
									$.each(variants_obj, function (j) {
										var txt_variants = json.data[i].translates[DefaultLanguage].optionTranslations.translations[variants_obj[j]];
										str += '<i>' + txt_variants + ' /</i>';
									});
								} else {
									str += '<i>&nbsp;</i>';
								}
								str += '</p>';
								str +=
									'<p class="writenow"><textarea name="" rows="" cols="" class="txtear"></textarea><button class="submitsub">Submit</button></p>';
								str += '<p class="writecopy cur">&nbsp;</p>';
								str += '</div>	';
								str += '<img src="images/icon_all/ArtboardCopy3x.png" class="delete" data-id="' + json.data[i].id +
									'" onclick="delete_shangpin(this);"/>';
								str += '<img src="images/icon_all/Artboard3x.png" class="write"/>';
								str += '<img src="images/icon_all/reduced3x.png" class="cartminus"  data-recoderid="' + json.data[i].id +
									'" data-price="' + json.data[i].price_in_mill + '" onclick="minus_num(this);"/>';
								str += '<span class="cartnum">' + json.data[i].quantity + '</span>';
								str += '<img src="images/icon_all/add3x.png" class="cartadd"  data-recoderid="' + json.data[i].id +
									'" data-price="' + json.data[i].price_in_mill + '" onclick="add_num(this);"/>	';
								str += '</li>';
				
								$(".prolist").append(str);
								$(".empty").show();
							});
				
							var total = 0;
							$.each(json.data, function (ii) {
								total += parseInt(json.data[ii].unit_price_in_mill) / 1000 * parseInt(json.data[ii].quantity);
								$(".cartli2 span").html("$" + total);
							});
					
			}else{
						console.log("cartinfo read error",err);
			}
			
		});

	} else {





		var url = API_URL + "cart.get_info&device_type=tablet&device_id=" + device_id + "&lang=" + DefaultLanguage;
		get_data(url).then(function (json) {
			/** 缓存数据**/

			localforage.setItem("cartinfo", json).then(function (jsonobj) {
				// 如下输出save_cartinfo
				console.log("save_cartinfo:", jsonobj);
			}).catch(function (err) {
				// 当出错时，此处代码运行
				console.log(err);
			});



			$(".prolist").empty();
			$.each(json.data, function (i) {
				var defaultImageid = json.data[i].defaultImage;
				var defaultImage = ((defaultImageid !== null) && imglist.data[defaultImageid]) ? imglist.data[defaultImageid].internal_image_url :
					imglist.data[1484].internal_image_url;
				var str = '';
				var variants_obj = JSON.parse(json.data[i].variants);
				str += '<li class="cartli0">';
				str += '<a href="detail.html?id=' + json.data[i].page_id + '&type=MERCHANDISE"><img src="' + defaultImage +
					'"/></a>';
				str += '<div id="">';
				str += '<p class="ordertitle">' + json.data[i].translates[DefaultLanguage].title + '<span class="liprice" >$' +
					parseFloat(json.data[i].price_in_mill) / 1000.0 + '</span></p>';
				str += '<p class="orderdetail">';
				//			从接口中获取商品附加属性并显示
				if (variants_obj !== null) {
					$.each(variants_obj, function (j) {
						var txt_variants = json.data[i].translates[DefaultLanguage].optionTranslations.translations[variants_obj[j]];
						str += '<i>' + txt_variants + ' /</i>';
					});
				} else {
					str += '<i>&nbsp;</i>';
				}
				str += '</p>';
				str +=
					'<p class="writenow"><textarea name="" rows="" cols="" class="txtear"></textarea><button class="submitsub">Submit</button></p>';
				str += '<p class="writecopy cur">&nbsp;</p>';
				str += '</div>	';
				str += '<img src="images/icon_all/ArtboardCopy3x.png" class="delete" data-id="' + json.data[i].id +
					'" onclick="delete_shangpin(this);"/>';
				str += '<img src="images/icon_all/Artboard3x.png" class="write"/>';
				str += '<img src="images/icon_all/reduced3x.png" class="cartminus"  data-recoderid="' + json.data[i].id +
					'" data-price="' + json.data[i].price_in_mill + '" onclick="minus_num(this);"/>';
				str += '<span class="cartnum">' + json.data[i].quantity + '</span>';
				str += '<img src="images/icon_all/add3x.png" class="cartadd"  data-recoderid="' + json.data[i].id +
					'" data-price="' + json.data[i].price_in_mill + '" onclick="add_num(this);"/>	';
				str += '</li>';

				$(".prolist").append(str);
				$(".empty").show();
			});

			var total = 0;
			$.each(json.data, function (ii) {
				total += parseInt(json.data[ii].unit_price_in_mill) / 1000 * parseInt(json.data[ii].quantity);
				$(".cartli2 span").html("$" + total);
			});

		}, function (json) {
			$(".empty").hide();
			$(".cart_empty").addClass("cur");

		});

	}

}
$(".empty_okbtn")[0].ontouchend = function (e) {
	e.preventDefault();
	$("#cartcon").removeClass("cur");
	$(".carticon").removeClass("cur");
}
//shoppingcart商品增删改查
//商品加一按钮
function add_num(data) {
	var num = parseInt($(data).parent().children(".cartnum").text());
	var price = $(data).data("price");
	num++;
	$(data).parent().children(".cartnum").text(num);

	var total = 0;
	total = parseInt($(".cartli2 span").html().replace("$", "")) + parseInt(price) / 1000;
	$(".cartli2 span").html("$" + total);
	var recoderid = $(data).data("recoderid");
	var Url = API_URL + "cart.edit&device_type=tablet&lang=" + DefaultLanguage;
	var datas = {
		"id": recoderid,
		"quantity": num,
		"price_in_mill": (parseInt(($(data).parent().find($(".liprice")).html()).split("$")[1]) * 1000) * num
	}
	post_data(datas, Url);
}
//商品减一按钮
function minus_num(data) {
	var num = parseInt($(data).parent().children(".cartnum").text());
	var price = $(data).data("price");
	if (num > 1) {
		num--;
		var total = 0;
		total = parseFloat($(".cartli2 span").html().replace("$", "")) - parseInt(price) / 1000;
		$(".cartli2 span").html("$" + total);
		var recoderid = $(data).data("recoderid");
		var Url = API_URL + "cart.edit&device_type=tablet&lang=" + DefaultLanguage;
		var datas = {
			"id": recoderid,
			"quantity": num,
			"price_in_mill": (parseInt(($(data).parent().find($(".liprice")).html()).split("$")[1]) * 1000) * num
		}
		post_data(datas, Url);
	}
	$(data).parent().children(".cartnum").text(num);
	$(data).parent().children(".cartnum").text(num);
}
var total = 0;

//点击购物车icon时，请求接口中数据并显示
$("#cart")[0].ontouchend = function (e) {
	e.preventDefault();
	$(".cartli2 span").html('');
	cartinfo();


	//	为其他icon初始化
	ordercon.className = "";
	ordericon[0].className = "ordericon";
	messagecon.className = "";
	messageicon[0].className = "messageicon";
	languagecon.className = "";
	$(".orderdetails").removeClass("cur");
	//	支付方式和详情
	$("#paymentmethod").removeClass("cur");
	$("#alipy").removeClass("cur");
	$("#wechat").removeClass("cur");
	$("#coupons").removeClass("cur");
	$("#cash").removeClass("cur");
	$("#creditcard").removeClass("cur");
	$("#roomfolio").removeClass("cur");
	//	初始化结束
	if (cartcon.className == "cur") {
		cartcon.className = "";
		carticon[0].className = "carticon";
	} else if (confirmorder.className == "cur") {
		confirmorder.className = "";
		carticon[0].className = "carticon";
	} else if ($(".carticon").hasClass("cur")) {
		paymentmethod.className = "";
		carticon[0].className = "carticon";
		$("#alipy").removeClass("cur");
		$("#wechat").removeClass("cur");
		$("#coupons").removeClass("cur");
		$("#cash").removeClass("cur");
		$("#creditcard").removeClass("cur");
		$("#roomfolio").removeClass("cur");
	} else {
		cartcon.className = "cur";
		carticon[0].className = "carticon cur";
	}
}

function get_orderlist(order_status) {
	var url = API_URL + "order.get_list&device_type=tablet&device_id=" + device_id + "&order_status=" + order_status +
		"&lang=" + DefaultLanguage;
	get_data(url).then(function (json) {
		//		console.log(json.data);
		var str = '';
		$.each(json.data, function (i) {
			switch (json.data[i].status) {
				//				订单未支付
				case "WAITING_PAYMENT":
					str += '<li>';
					str +=
						'<div class="sellertxt"><span class="sellername">Haven Hotel</span><span class="state">Unpaid</span></div>';
					str += '<div class="sellerimg">';
					//					如果每单商品过多,过滤只显示前三个商品的图片
					if (json.data[i].items.length > 4) {
						for (var j = 0; j < 4; j++) {
							var defaultImage = (typeof (json.data[i].items[j].defaultImage) !== undefined) ? imglist.data[json.data[i].items[
								j].defaultImage].internal_image_url : "images/default.png";
							str += '<img src="' + defaultImage + '" data-orderid="' + json.data[i].internalOrderId + '" data-status="' +
								json.data[i].status + '" onclick="toorderdetail(this);"/>';
						}
					} else {
						$.each(json.data[i].items, function (j) {
							var defaultImage = (typeof (json.data[i].items[j].defaultImage) !== undefined) ? imglist.data[json.data[i]
								.items[j].defaultImage].internal_image_url : "images/default.png";
							str += '<img src="' + defaultImage + '" data-orderid="' + json.data[i].internalOrderId + '" data-status="' +
								json.data[i].status + '" onclick="toorderdetail(this);"/>';
						});
					}
					str += '<button class="cancelled_order" onclick="cancelledorder(this);" data-orderid="' + json.data[i].internalOrderId +
						'">cancel</button>';
					str += '<button class="paynow_btn" onclick="paynow0(this);" data-orderid="' + json.data[i].internalOrderId +
						'">Pay Now</button>';
					str += '</div>';
					str += '<div class="statetxt">';
					str += '<div class="flleft">';
					str += '<p>Order Number：<span>' + json.data[i].internalOrderId + '</span></p>';
					str += '<p>Order Time：<span>2017-10-20 11:26:45</span></p>';
					str += '</div>';
					str += '<div>';
					str += '<p>Total Items: <span>' + json.data[i].items.length + '</span></p><br />';
					str += '<p>Amount:<span data-ordertotal="' + json.data[i].internalOrderId + '">$' + parseInt(json.data[i].totalPriceInMill) /
						1000 + '</span></p>';
					str += '</div>';
					str += '</div>';
					str += '</li>';
					$("ul.unpaid").html(str);
					break;
					//				订单正在处理
				case "PROCESSING":
					str += '<li>';
					str +=
						'<div class="sellertxt"><span class="sellername">Haven Hotel</span><span class="state">Processing</span></div>';
					str += '<div class="sellerimg">';
					if (json.data[i].items.length > 4) {
						for (var j = 0; j < 4; j++) {
							var defaultImage = (typeof (json.data[i].items[j].defaultImage) !== undefined) ? imglist.data[json.data[i].items[
								j].defaultImage].internal_image_url : "images/default.png";
							str += '<img src="' + defaultImage + '" data-orderid="' + json.data[i].internalOrderId + '" data-status="' +
								json.data[i].status + '" onclick="toorderdetail(this);"/>';
						}
					} else {
						$.each(json.data[i].items, function (j) {
							var defaultImage = (typeof (json.data[i].items[j].defaultImage) !== undefined) ? imglist.data[json.data[i]
								.items[j].defaultImage].internal_image_url : "images/default.png";
							str += '<img src="' + defaultImage + '" data-orderid="' + json.data[i].internalOrderId + '" data-status="' +
								json.data[i].status + '" onclick="toorderdetail(this);"/>';
						});
					}
					str += '<button class="" onclick="cancelledorder(this);" data-orderid="' + json.data[i].internalOrderId +
						'">cancel</button>';
					str += '</div>';
					str += '<div class="statetxt">';
					str += '<div class="flleft">';
					str += '<p>Order Number：<span>' + json.data[i].internalOrderId + '</span></p>';
					str += '<p>Order Time：<span>2017-10-20 11:26:45</span></p>';
					str += '</div>';
					str += '<div>';
					str += '<p>Total Items: <span>' + json.data[i].items.length + '</span></p><br />';
					str += '<p>Amount:<span>$' + parseInt(json.data[i].totalPriceInMill) / 1000 + '</span></p>';
					str += '</div>';
					str += '</div>';
					str += '</li>';
					$("ul.processing").html(str);
					break;
					//				订单已付款，未处理
				case "STARTED":
					str += '<li>';
					str +=
						'<div class="sellertxt"><span class="sellername">Haven Hotel</span><span class="state">Unprocessed</span></div>';
					str += '<div class="sellerimg">';
					if (json.data[i].items.length > 4) {
						for (var j = 0; j < 4; j++) {
							var defaultImage = (typeof (json.data[i].items[j].defaultImage) !== undefined) ? imglist.data[json.data[i].items[
								j].defaultImage].internal_image_url : "images/default.png";
							str += '<img src="' + defaultImage + '" data-orderid="' + json.data[i].internalOrderId + '" data-status="' +
								json.data[i].status + '" onclick="toorderdetail(this);"/>';
						}
					} else {
						$.each(json.data[i].items, function (j) {
							var defaultImage = (typeof (json.data[i].items[j].defaultImage) !== undefined) ? imglist.data[json.data[i]
								.items[j].defaultImage].internal_image_url : "images/default.png";
							str += '<img src="' + defaultImage + '" data-orderid="' + json.data[i].internalOrderId + '" data-status="' +
								json.data[i].status + '" onclick="toorderdetail(this);"/>';
						});
					}
					str += '<button class="" onclick="cancelledorder(this);" data-orderid="' + json.data[i].internalOrderId +
						'">cancel</button>';
					str += '</div>';
					str += '<div class="statetxt">';
					str += '<div class="flleft">';
					str += '<p>Order Number：<span>' + json.data[i].internalOrderId + '</span></p>';
					str += '<p>Order Time：<span>2017-10-20 11:26:45</span></p>';
					str += '</div>';
					str += '<div>';
					str += '<p>Total Items: <span>' + json.data[i].items.length + '</span></p><br />';
					str += '<p>Amount:<span>$' + parseInt(json.data[i].totalPriceInMill) / 1000 + '</span></p>';
					str += '</div>';
					str += '</div>';
					str += '</li>';
					$("ul.unprocessed").html(str);
					break;
					//				订单已完成
				case "PAID":
					str += '<li>';
					str +=
						'<div class="sellertxt"><span class="sellername">Haven Hotel</span><span class="state">Completed</span></div>';
					str += '<div class="sellerimg">';
					if (json.data[i].items.length > 4) {
						for (var j = 0; j < 4; j++) {
							var defaultImage = (typeof (json.data[i].items[j].defaultImage) !== undefined) ? imglist.data[json.data[i].items[
								j].defaultImage].internal_image_url : "images/default.png";
							str += '<img src="' + defaultImage + '" data-orderid="' + json.data[i].internalOrderId + '" data-status="' +
								json.data[i].status + '" onclick="toorderdetail(this);"/>';
						}
					} else {
						$.each(json.data[i].items, function (j) {
							var defaultImage = (typeof (json.data[i].items[j].defaultImage) !== undefined) ? imglist.data[json.data[i]
								.items[j].defaultImage].internal_image_url : "images/default.png";
							str += '<img src="' + defaultImage + '" data-orderid="' + json.data[i].internalOrderId + '" data-status="' +
								json.data[i].status + '" onclick="toorderdetail(this);"/>';
						});
					}
					str += '</div>';
					str += '<div class="statetxt">';
					str += '<div class="flleft">';
					str += '<p>Order Number：<span>' + json.data[i].internalOrderId + '</span></p>';
					str += '<p>Order Time：<span>2017-10-20 11:26:45</span></p>';
					str += '</div>';
					str += '<div>';
					str += '<p>Total Items: <span>' + json.data[i].items.length + '</span></p><br />';
					str += '<p>Amount:<span>$' + parseInt(json.data[i].totalPriceInMill) / 1000 + '</span></p>';
					str += '</div>';
					str += '</div>';
					str += '</li>';
					$("ul.completed").html(str);
					break;
				default:

					break;
			}

		});

	});
}
//订单模块
$("#order")[0].ontouchend = function (e) {
	e.preventDefault();
	get_orderlist($("#ordertitle .cur").data("status"));
	//	为其他icon初始化
	cartcon.className = "";
	carticon[0].className = "carticon";
	messagecon.className = "";
	messageicon[0].className = "messageicon";
	languagecon.className = "";
	//	支付方式和详情
	$("#alipy").removeClass("cur");
	$("#wechat").removeClass("cur");
	$("#coupons").removeClass("cur");
	$("#cash").removeClass("cur");
	$("#creditcard").removeClass("cur");
	$("#roomfolio").removeClass("cur");
	paymentmethod.className = "";
	//	初始化结束
	if (ordercon.className == "cur") {
		ordercon.className = "";
		ordericon[0].className = "ordericon";
	} else if ($(".orderdetails").hasClass("cur")) {
		$(".orderdetails").removeClass("cur");
		ordericon[0].className = "ordericon";
	} else if ($(".ordericon").hasClass("cur")) {
		$(".ordericon").removeClass("cur");
		$("#paymentmethod").removeClass("cur");
		$("#alipy").removeClass("cur");
		$("#wechat").removeClass("cur");
		$("#coupons").removeClass("cur");
		$("#cash").removeClass("cur");
		$("#creditcard").removeClass("cur");
		$("#roomfolio").removeClass("cur");
	} else {
		ordercon.className = "cur";
		ordericon[0].className = "ordericon cur";
	}
}
//to orderdetail函数
function toorderdetail(trag) {
	$("#ordercon").removeClass("cur");
	$(".orderdetails").addClass("cur");
	var str = '';
	var url = API_URL + "order.get_info&device_type=tablet&device_id=" + device_id + "&order_id=" + $(trag).data('orderid') +
		"&lang=" + DefaultLanguage;
	get_data(url).then(function (json) {
		//		console.log(toDecimal2(parseInt(json.data.items[i].priceInMill)))

		switch (json.data.status) {

			case "WAITING_PAYMENT":
				str += '<div class="div1">';
				str += '	<p class="floatleft"><img src="images/map.png" alt="" />&nbsp; Room: 102</p>';
				str += '	<p class="floatright">Order Number：<span>' + json.data.internalOrderId + '</span></p><br />';
				str += '	<p class="floatright ordertime">Order Time：<span>2017-10-18 11:26:45</span></p>';
				str += '</div>';
				str += '<div class="div2">';
				str +=
					'	<p><span class="floatleft">Haven Hotel</span><span class="floatright orderstate" style="color:#d33c01">Unpaid</span></p>';
				str += '	<ul>';
				$.each(json.data.items, function (i) {
					var defaultImage = (typeof (json.data.items[i].defaultImage) !== undefined) ? imglist.data[json.data.items[i].defaultImage]
						.internal_image_url : "images/default.png";
					var variants = json.data.items[0].variants;
					str += '<li>';
					str += '<a href="detail.html"><img src="' + defaultImage + '"/></a>';
					str += '<div id="">';
					str += '<p class="name_p"><span>' + json.data.items[i].translates[DefaultLanguage].title + '</span></p>';
					str += '<p class="num_p">x';
					str += '<sapn>' + json.data.items[i].quantity + '</sapn>';
					str += '</p>';
					str += '<p class="price_p"><span>' + StrToMoney(toDecimal2(parseInt(json.data.items[i].priceInMill))) +
						'</span></p>';
					//					str +='<p class="detail_p">'+json.data.items[i].translates[DefaultLanguage].optionTranslations.translations.variants+'</p>';
					str += '<p class="beizhu_p"> </p>';
					str += '</div>';
					str += '</li>';
				});
				str += '	</ul>';
				str += '</div>';
				str += '<div class="div4">';
				str += '	<p><span class="floatleft">Subtotal</span><span class="floatright subtotal_span">' + StrToMoney(
					toDecimal2(parseInt(json.data.totalPriceInMill))) + '</span></p><br />';
				str += '	<p><span class="floatleft">Service charge (15%)</span><span class="floatright service_span">' +
					StrToMoney(toDecimal2(parseInt(json.data.totalPriceInMill) * parseFloat(surchargenum))) + '</span></p><br />';
				str += '	<p class="total_amount">Total Amount: <span>' + StrToMoney(toDecimal2(((parseInt(json.data.totalPriceInMill) *
					parseFloat(surchargenum)) + parseInt(json.data.totalPriceInMill)))) + '</span></p>';
				str +=
					'	<p class="unpaid_btn"><button class="cancle_order" onclick="cancle_order();">Cancle Order</button><button class="unpaynow_btn" onclick="paynowdetail();">Pay Now</button></p>';
				str += '</div>';

				$(".orderdetails").find("h3").eq(0).after(str);
				break;
			case "PROCESSING":
				str += '<div class="div1">';
				str += '	<p class="floatleft"><img src="images/map.png" alt="" />&nbsp; Room: 102</p>';
				str += '	<p class="floatright">Order Number：<span>' + json.data.internalOrderId + '</span></p><br />';
				str += '	<p class="floatright ordertime">Order Time：<span>2017-10-18 11:26:45</span></p>';
				str += '</div>';
				str += '<div class="div2">';
				str +=
					'	<p><span class="floatleft">Haven Hotel</span><span class="floatright orderstate" style="color:#d33c01">Unpaid</span></p>';
				str += '	<ul>';
				$.each(json.data.items, function (i) {
					var defaultImage = (typeof (json.data.items[i].defaultImage) !== undefined) ? imglist.data[json.data.items[i].defaultImage]
						.internal_image_url : "images/default.png";
					var variants = json.data.items[0].variants;
					str += '<li>';
					str += '<a href="detail.html"><img src="' + defaultImage + '"/></a>';
					str += '<div id="">';
					str += '<p class="name_p"><span>' + json.data.items[i].translates[DefaultLanguage].title + '</span></p>';
					str += '<p class="num_p">x';
					str += '<sapn>' + json.data.items[i].quantity + '</sapn>';
					str += '</p>';
					str += '<p class="price_p"><span>' + StrToMoney(toDecimal2(parseInt(json.data.items[i].priceInMill))) +
						'</span></p>';
					//					str +='<p class="detail_p">'+json.data.items[i].translates[DefaultLanguage].optionTranslations.translations.variants+'</p>';
					str += '<p class="beizhu_p"> </p>';
					str += '</div>';
					str += '</li>';
				});
				str += '	</ul>';
				str += '</div>';
				str += '<div class="div4">';
				str += '	<p><span class="floatleft">Subtotal</span><span class="floatright subtotal_span">' + StrToMoney(
					toDecimal2(parseInt(json.data.totalPriceInMill))) + '</span></p><br />';
				str += '	<p><span class="floatleft">Service charge (15%)</span><span class="floatright service_span">' +
					StrToMoney(toDecimal2(parseInt(json.data.totalPriceInMill) * parseFloat(surchargenum))) + '</span></p><br />';
				str += '	<p class="total_amount">Total Amount: <span>' + StrToMoney(toDecimal2(((parseInt(json.data.totalPriceInMill) *
					parseFloat(surchargenum)) + parseInt(json.data.totalPriceInMill)))) + '</span></p>';
				str += '	<p class="unpaid_btn"><button class="cancle_order" onclick="cancle_order();">Cancle Order</button></p>';
				str += '</div>';

				$(".orderdetails").find("h3").eq(0).after(str);
				break;
			case "STARTED":

				str += '<div class="div1">';
				str += '	<p class="floatleft"><img src="images/map.png" alt="" />&nbsp; Room: 102</p>';
				str += '	<p class="floatright">Order Number：<span>' + json.data.internalOrderId + '</span></p><br />';
				str += '	<p class="floatright ordertime">Order Time：<span>2017-10-18 11:26:45</span></p>';
				str += '</div>';
				str += '<div class="div2">';
				str +=
					'	<p><span class="floatleft">Haven Hotel</span><span class="floatright orderstate" style="color:#d33c01">Unpaid</span></p>';
				str += '	<ul>';
				$.each(json.data.items, function (i) {
					var defaultImage = (typeof (json.data.items[i].defaultImage) !== undefined) ? imglist.data[json.data.items[i].defaultImage]
						.internal_image_url : "images/default.png";
					var variants = json.data.items[0].variants;
					str += '<li>';
					str += '<a href="detail.html"><img src="' + defaultImage + '"/></a>';
					str += '<div id="">';
					str += '<p class="name_p"><span>' + json.data.items[i].translates[DefaultLanguage].title + '</span></p>';
					str += '<p class="num_p">x';
					str += '<sapn>' + json.data.items[i].quantity + '</sapn>';
					str += '</p>';
					str += '<p class="price_p"><span>' + StrToMoney(toDecimal2(parseInt(json.data.items[i].priceInMill))) +
						'</span></p>';
					//					str +='<p class="detail_p">'+json.data.items[i].translates[DefaultLanguage].optionTranslations.translations.variants+'</p>';
					str += '<p class="beizhu_p"> </p>';
					str += '</div>';
					str += '</li>';
				});
				str += '	</ul>';
				str += '</div>';
				str += '<div class="div4">';
				str += '	<p><span class="floatleft">Subtotal</span><span class="floatright subtotal_span">' + StrToMoney(
					toDecimal2(parseInt(json.data.totalPriceInMill))) + '</span></p><br />';
				str += '	<p><span class="floatleft">Service charge (15%)</span><span class="floatright service_span">' +
					StrToMoney(toDecimal2(parseInt(json.data.totalPriceInMill) * parseFloat(surchargenum))) + '</span></p><br />';
				str += '	<p class="total_amount">Total Amount: <span>' + StrToMoney(toDecimal2(((parseInt(json.data.totalPriceInMill) *
					parseFloat(surchargenum)) + parseInt(json.data.totalPriceInMill)))) + '</span></p>';
				str += '	<p class="unpaid_btn"><button class="cancle_order" onclick="cancle_order();">Cancle Order</button></p>';
				str += '</div>';

				$(".orderdetails").find("h3").eq(0).after(str);
				break;
			case "PAID":
				str += '<div class="div1">';
				str += '	<p class="floatleft"><img src="images/map.png" alt="" />&nbsp; Room: 102</p>';
				str += '	<p class="floatright">Order Number：<span>' + json.data.internalOrderId + '</span></p><br />';
				str += '	<p class="floatright ordertime">Order Time：<span>2017-10-18 11:26:45</span></p>';
				str += '</div>';
				str += '<div class="div2">';
				str +=
					'	<p><span class="floatleft">Haven Hotel</span><span class="floatright orderstate" style="color:#d33c01">Unpaid</span></p>';
				str += '	<ul>';
				$.each(json.data.items, function (i) {
					var defaultImage = (typeof (json.data.items[i].defaultImage) !== undefined) ? imglist.data[json.data.items[i].defaultImage]
						.internal_image_url : "images/default.png";
					var variants = json.data.items[0].variants;
					str += '<li>';
					str += '<a href="detail.html"><img src="' + defaultImage + '"/></a>';
					str += '<div id="">';
					str += '<p class="name_p"><span>' + json.data.items[i].translates[DefaultLanguage].title + '</span></p>';
					str += '<p class="num_p">x';
					str += '<sapn>' + json.data.items[i].quantity + '</sapn>';
					str += '</p>';
					str += '<p class="price_p"><span>' + StrToMoney(toDecimal2(parseInt(json.data.items[i].priceInMill))) +
						'</span></p>';
					//					str +='<p class="detail_p">'+json.data.items[i].translates[DefaultLanguage].optionTranslations.translations.variants+'</p>';
					str += '<p class="beizhu_p"> </p>';
					str += '</div>';
					str += '</li>';
				});
				str += '	</ul>';
				str += '</div>';
				str += '<div class="div3">';
				str += '	<p><span class="floatleft">Payment method：</span><span class="floatright">Alipy</span></p><br />';
				str += '	<p><span class="floatleft">Service Time：</span><span class="floatright">2017-10-18  12:26:45</span></p>';
				str += '</div>';
				str += '<div class="div4">';
				str += '	<p><span class="floatleft">Subtotal</span><span class="floatright subtotal_span">' + StrToMoney(
					toDecimal2(parseInt(json.data.totalPriceInMill))) + '</span></p><br />';
				str += '	<p><span class="floatleft">Service charge (15%)</span><span class="floatright service_span">' +
					StrToMoney(toDecimal2(parseInt(json.data.totalPriceInMill) * parseFloat(surchargenum))) + '</span></p><br />';
				str += '	<p class="total_amount">Total Amount: <span>' + StrToMoney(toDecimal2(((parseInt(json.data.totalPriceInMill) *
					parseFloat(surchargenum)) + parseInt(json.data.totalPriceInMill)))) + '</span></p>';
				//				str +='	<p class="unpaid_btn"><button class="cancle_order" onclick="cancle_order();">Cancle Order</button><button class="unpaynow_btn" onclick="paynowdetail();">Pay Now</button></p>';
				str += '</div>';

				$(".orderdetails").find("h3").eq(0).after(str);
				break;
			default:

				break;

		}
	});

}
//点击订单列表上的取消按钮，调订单取消接口，并删除该订单的dom元素
function cancelledorder(trag) {
	var url = API_URL + "order.cancel&device_type=tablet&device_id=" + device_id + "&order_id=" + $(trag).data('orderid') +
		"&lang=" + DefaultLanguage;
	get_data(url).then(function (json) {
		if (json.data.result == "OK") {
			//			console.log($(trag).parent().parent().remove());
			$(trag).parent().parent().remove();
		}
	})

}
//点击cancel按钮
function cancel(trag) {
	$(trag).parent().removeClass("cur");
	$(".ordericon").removeClass("cur");
	$(".carticon").removeClass("cur");
}

//信息推送
$("#message")[0].ontouchend = function (e) {
	e.preventDefault();
	//	为其他icon初始化
	cartcon.className = "";
	carticon[0].className = "carticon";
	ordercon.className = "";
	ordericon[0].className = "ordericon";
	languagecon.className = "";
	//	支付方式和详情
	$("#alipy").removeClass("cur");
	$("#wechat").removeClass("cur");
	$("#coupons").removeClass("cur");
	$("#cash").removeClass("cur");
	$("#creditcard").removeClass("cur");
	$("#roomfolio").removeClass("cur");
	paymentmethod.className = "";
	$(".orderdetails").removeClass("cur");
	//	初始化结束
	if (messagecon.className == "cur") {
		messagecon.className = "";
		messageicon[0].className = "messageicon";
	} else {
		messagecon.className = "cur";
		messageicon[0].className = "messageicon cur";
	}
}
//消息删除按钮
$.each(delmgs, function (j) {
	delmgs[j].ontouchend = function (e) {
		e.preventDefault();
		this.parentNode.parentNode.removeChild(this.parentNode);
	}
})
//语言切换
$("#language")[0].ontouchend = function (e) {
	e.preventDefault();
	//	为其他icon初始化
	cartcon.className = "";
	carticon[0].className = "carticon";
	ordercon.className = "";
	ordericon[0].className = "ordericon";
	messagecon.className = "";
	messageicon[0].className = "messageicon";
	//	支付方式和详情
	$("#alipy").removeClass("cur");
	$("#wechat").removeClass("cur");
	$("#coupons").removeClass("cur");
	$("#cash").removeClass("cur");
	$("#creditcard").removeClass("cur");
	$("#roomfolio").removeClass("cur");
	paymentmethod.className = "";
	$(".orderdetails").removeClass("cur");
	//	初始化结束
	if (languagecon.className == "cur") {
		languagecon.className = "";
	} else {
		languagecon.className = "cur";
	}
}
//myorder tab
function order_tab(trag) {
	var status = $(trag).data("status");
	$("#ordertitle span").removeClass("cur");
	$(trag).addClass("cur");
	get_orderlist($(trag).data("status"));
	$("#ordercon ul").removeClass("cur");
	$("ul[data-status='" + status + "']").addClass("cur");
}
//点击orderList中的某个跳到orderdetail
for (var i = 0; i < $(".sellerimg img").length; i++) {
	$(".sellerimg img")[i].ontouchend = function (e) {
		e.preventDefault();
		$("#ordercon").removeClass("cur");
		$(".orderdetails").addClass("cur");
	}
}
$(".return_orderlist")[0].ontouchend = function (e) {
	e.preventDefault();
	$(".orderdetails").removeClass("cur");
	$("#ordercon").addClass("cur");
}
//点击orderdetails上的cancle order关闭弹框
function cancle_order() {
	$(".orderdetails").removeClass("cur");
	$(".ordericon").removeClass("cur");
}
//点击orderdetails上的pay now按钮
function paynowdetail() {
	$(".orderdetails").removeClass("cur");
	$("#paymentmethod").addClass("cur");
}
//点击OK键时弹框消失
$("#coupons .ok")[0].ontouchend = function (e) {
	e.preventDefault();
	$("#coupons").removeClass("cur");
	$(".ordericon").removeClass("cur");
	$(".carticon").removeClass("cur");
}
$("#cash .ok")[0].ontouchend = function (e) {
	e.preventDefault();
	$("#cash").removeClass("cur");
	$(".ordericon").removeClass("cur");
	$(".carticon").removeClass("cur");
}
$("#creditcard .ok")[0].ontouchend = function (e) {
	e.preventDefault();
	$("#creditcard").removeClass("cur");
	$(".ordericon").removeClass("cur");
	$(".carticon").removeClass("cur");
}
$("#roomfolio .ok")[0].ontouchend = function (e) {
	e.preventDefault();
	$("#roomfolio").removeClass("cur");
	$(".ordericon").removeClass("cur");
	$(".carticon").removeClass("cur");
}
//点击购物车为空时的ok按钮弹框消失
if ($(".empty_okbtn").hasClass("cur")) {
	$(".empty_okbtn")[0].ontouchend = function (e) {
		e.preventDefault();
		$(".cart_empty").removeClass("cur");
		$(".carticon").removeClass("cur");
	}
}
//点击订单列表上的pay now按钮跳出付款页面

function paynow0(trag) {
	$("#paymentmethod").addClass("cur");
	$("#ordercon").removeClass("cur");
	ORDERID = $(trag).data("orderid");
	$("#payment").data('orderid', ORDERID);
}
//点击confirmbtn0事件购物车确认页面
confirmbtn0.ontouchend = function (e) {
	e.preventDefault();
	confirmorder.className = "cur";
	cartcon.className = "";
	//	购物车中整个订单的备注传入订单页面
	$(".order_request>p span").html($("#additional").val());
	
	if(OFFLINE){
		
		localforage.getItem("cartdetail",function(err,json){
			if(err==null && json !== null){
				if (json.ret == 200) {
					var strorder = '';
					var totalorder = 0;
					strorder += '<li class="ultitle">';
					strorder +=
						'<span class="item_detail">Item Details</span><span class="qty">QTY</span><span class="ul_price">Price</span><span class="total">Total</span>';
					strorder += '</li>';
					$.each(json.data, function (i) {
						var variants_obj = JSON.parse(json.data[i].variants);
						var strvariants = '';
						//			从接口中获取商品附加属性并显示			
						if (variants_obj !== null) {
							$.each(variants_obj, function (j) {
								var txt_variants = json.data[i].translates[DefaultLanguage].optionTranslations.translations[variants_obj[j]];
								strvariants += '<i>' + txt_variants + ' /</i>';
							});
						}
						//			循环商品列表并写入dom
						strorder += '<li>';
						strorder += '<p class="lititle"><span>' + (i + 1) + '.&nbsp;&nbsp;' + json.data[i].translates[DefaultLanguage]
							.title + '</span><span class="qty">' + json.data[i].quantity + '</span><span class="ul_price">$' + parseInt(
								json.data[i].unit_price_in_mill) / 1000.0 + '</span><span class="total">$' + (parseInt(json.data[i].quantity) *
								parseInt(json.data[i].unit_price_in_mill)) / 1000.0 + '</span></p>';
						strorder += '<p>' + strvariants + '</p>';
						strorder += '</li>';
						//			计算商品总价并写入dom
						totalorder += (parseInt(json.data[i].quantity) * parseInt(json.data[i].price_in_mill)) / 1000.0;
						$(".subtotal .floatright").html("$" + totalorder);
					});
					$("#confirmorder ul").html(strorder);
					//		根据商品总价计算服务费的费用并写入dom
					var subtotal = $(".subtotal .floatright").html();
					var surcharge = parseInt(parseFloat(subtotal.replace("$", "")) * surchargenum);
					$(".surcharge .floatright").html("$" + surcharge);
					//		计算整个订单的总价并写入dom
					$(".totalamount .floatright").html("$" + (parseInt(subtotal.replace("$", "")) + surcharge));
				}
			}else{
				console.log("read_cartdetail_err",err);
			}
			
		})
		
	}else{
		

	
	
	
	
	//	接口对接部分
	var url = API_URL + "cart.get_info&device_type=tablet&device_id=" + device_id + "&lang=" + DefaultLanguage;
	get_data(url).then(function (json) {
		
		//获取购物车详情，缓存到浏览器本地存储;
		localforage.setItem("cartdetail", json).then(function (jsonobj) {
			// 如下输出
			console.log("缓存成功:", jsonobj);
		}).catch(function (err) {
			// 当出错时，此处代码运行
			console.log(err);
		});
		
		
		if (json.ret == 200) {
			var strorder = '';
			var totalorder = 0;
			strorder += '<li class="ultitle">';
			strorder +=
				'<span class="item_detail">Item Details</span><span class="qty">QTY</span><span class="ul_price">Price</span><span class="total">Total</span>';
			strorder += '</li>';
			$.each(json.data, function (i) {
				var variants_obj = JSON.parse(json.data[i].variants);
				var strvariants = '';
				//			从接口中获取商品附加属性并显示			
				if (variants_obj !== null) {
					$.each(variants_obj, function (j) {
						var txt_variants = json.data[i].translates[DefaultLanguage].optionTranslations.translations[variants_obj[j]];
						strvariants += '<i>' + txt_variants + ' /</i>';
					});
				}
				//			循环商品列表并写入dom
				strorder += '<li>';
				strorder += '<p class="lititle"><span>' + (i + 1) + '.&nbsp;&nbsp;' + json.data[i].translates[DefaultLanguage]
					.title + '</span><span class="qty">' + json.data[i].quantity + '</span><span class="ul_price">$' + parseInt(
						json.data[i].unit_price_in_mill) / 1000.0 + '</span><span class="total">$' + (parseInt(json.data[i].quantity) *
						parseInt(json.data[i].unit_price_in_mill)) / 1000.0 + '</span></p>';
				strorder += '<p>' + strvariants + '</p>';
				strorder += '</li>';
				//			计算商品总价并写入dom
				totalorder += (parseInt(json.data[i].quantity) * parseInt(json.data[i].price_in_mill)) / 1000.0;
				$(".subtotal .floatright").html("$" + totalorder);
			});
			$("#confirmorder ul").html(strorder);
			//		根据商品总价计算服务费的费用并写入dom
			var subtotal = $(".subtotal .floatright").html();
			var surcharge = parseInt(parseFloat(subtotal.replace("$", "")) * surchargenum);
			$(".surcharge .floatright").html("$" + surcharge);
			//		计算整个订单的总价并写入dom
			$(".totalamount .floatright").html("$" + (parseInt(subtotal.replace("$", "")) + surcharge));
		}
	});
	
	}
}
//点击return1事件
return1.ontouchend = function (e) {
	e.preventDefault();
	cartcon.className = "cur";
	confirmorder.className = "";
	additional.value = '';
}
//点击confirmbtn1事件购物车确认页面的按钮
confirmbtn1.ontouchend = function (e) {
	e.preventDefault();
	paymentmethod.className = "cur";
	confirmorder.className = "";

	//	确认下单向后台传数据
	var _obj = {
		hotel_id: hotel_id,
		restaurant_id: restaurant_id,
		room_id: room_id,
		device_id: device_id,
		total_price_in_mill: parseInt(($(".totalamount .floatright").html()).split("$")[1]) * 1000,
		service_fee: surchargePermillage
	};
	var Url = API_URL + "order.add&device_type=tablet&lang=" + DefaultLanguage;

	post_data(_obj, Url).then(function (json) {

		if (json.result === "OK") {
			$("#payment").data('orderid', json.order_id);
			var url = API_URL + "cart.clear&device_type=tablet&device_id=" + device_id + "&room_id=" + room_id + "&lang=" +
				DefaultLanguage;
			get_data(url).then(function (json) {
				$(".prolist .cartli0").remove();
				$("div.cart_empty").addClass("cur");
				$("#carticonnum").html("0");
				$("#carticonnum").addClass("cur");
			})
		}
	});
}
//点击return2事件
return2.ontouchend = function (e) {
	e.preventDefault();
	if ($(".carticon").hasClass("cur")) {
		confirmorder.className = "cur";
		paymentmethod.className = "";
	} else if ($(".ordericon").hasClass("cur")) {
		$(".orderdetails").addClass("cur");
		paymentmethod.className = "";
	}
}
//点击return3事件
return3.ontouchend = function (e) {
	e.preventDefault();
	paymentmethod.className = "cur";
	alipy.className = "";
}
// 选择付款方式的排他
for (var p = 0; p < paydivs.length; p++) {
	paydivs[p].index = p;
	paydivs[p].ontouchend = function (e) {
		e.preventDefault();
		for (var q = 0; q < paydivs.length; q++) {
			paydivs[q].className = '';
		}
		this.className = 'cur';
	}
}
//支付方式为支付宝
$(".alipydiv")[0].ontouchend = function (e) {
	e.preventDefault();
	$(".erweima img").attr("src", "");
	$("#paymentmethod").removeClass("cur");
	$("#alipy").addClass("cur");
	var orderids = $("#payment").data('orderid');
	var pay_method = "ALI";
	var url = API_URL + "order.pay&device_type=tablet&lang=" + DefaultLanguage;
	var datas = {
		"order_id": orderids,
		"pay_method": pay_method,
		"device_id": device_id
	}
	console.log(orderids)
	//	获取二维码接口		
	post_data(datas, url).then(function (json) {
		//		console.log(json.qr_code);
		$("#qrcode img").remove();
		new QRCode(document.getElementById("qrcode"), json.qr_code);
		//		$("#qrcode img").find("src","").remove();
	});
	var timer = window.setInterval(function paystatus() {
		var url0 = API_URL + "order.get_payinfo&device_type=tablet&order_id=" + orderids + "&device_id=" + device_id +
			"&lang=" + DefaultLanguage;
		get_data(url0).then(function (json0) {
			//					支付成功
			if (json0.data.result == "OK" && json0.data.is_paid == true) {
				window.clearInterval(timer);
				var datas = {
					"pay_ali": json0.data.payment.ali
				}
				var url1 = API_URL + "order.complete_pay&device_type=tablet&order_id=" + orderids + "&device_id=" + device_id +
					"&lang=" + DefaultLanguage;
				post_data(datas, url1).then(function (json1) {
					if (json1.result == "OK") {
						$("#alipy").removeClass("cur");
						$("#paysuccess").addClass("cur");
					}
				});
			} else if (json0.data.result !== "OK") {

				$("#alipy").removeClass("cur");
				$("#payfailure").addClass("cur");
			}
		});
	}, 2000);
}
$(".return3")[0].ontouchend = function (e) {
	e.preventDefault();
	$("#paymentmethod").addClass("cur");
	$("#alipy").removeClass("cur");
}
//支付方式为微信
$(".wechatdiv")[0].ontouchend = function (e) {
	e.preventDefault();
	$(".erweima img").attr("src", "");
	$("#paymentmethod").removeClass("cur");
	$("#wechat").addClass("cur");
	var orderids = $("#payment").data('orderid');
	var pay_method = "WECHAT";
	var url = API_URL + "order.pay&device_type=tablet&lang=" + DefaultLanguage;
	var datas = {
		"order_id": orderids,
		"pay_method": pay_method,
		"device_id": device_id
	}
	//	获取二维码接口
	post_data(datas, url).then(function (json) {
		$("#qrcodewx img").remove();
		new QRCode(document.getElementById("qrcodewx"), json.qr_code);
	});
	var timer = window.setInterval(function paystatus() {
		var url0 = API_URL + "order.get_payinfo&device_type=tablet&order_id=" + orderids + "&device_id=" + device_id +
			"&lang=" + DefaultLanguage;
		get_data(url0).then(function (json0) {
			//			支付成功
			if (json0.data.result == "OK" && json0.data.is_paid == true) {
				window.clearInterval(timer);
				var datas = {
					"pay_ali": json0.data.payment.ali
				}
				var url1 = API_URL + "order.complete_pay&device_type=tablet&order_id=" + orderids + "&device_id=" + device_id +
					"&lang=" + DefaultLanguage;
				post_data(datas, url1).then(function (json1) {
					if (json1.result == "OK") {
						$("#alipy").removeClass("cur");
						$("#paysuccess").addClass("cur");
					}
				});
			} else if (json0.data.result !== "OK") {
				$("#alipy").removeClass("cur");
				$("#payfailure").addClass("cur");
			}
		});
	}, 2000);
}
$(".return3")[1].ontouchend = function (e) {
	e.preventDefault();
	$("#paymentmethod").addClass("cur");
	$("#wechat").removeClass("cur");
}
//支付方式为现金类的
$(".couponsdiv")[0].ontouchend = function (e) {
	e.preventDefault();
	$("#paymentmethod").removeClass("cur");
	$("#coupons").addClass("cur");
	var orderids = $("#payment").data('orderid');
	var pay_cash = parseInt(($("span[data-ordertotal='" + orderids + "']").html()).split("$")[1]) * 1000;

	var datas = {
		"pay_cash": pay_cash
	}
	var orderids = $("#payment").data('orderid');
	var url = API_URL + "order.complete_pay&device_type=tablet&order_id=" + orderids + "&device_id=" + device_id +
		"&lang=" + DefaultLanguage;
	post_data(datas, url).then(function (json) {
		//		console.log(json);
	});
}
$(".return3")[2].ontouchend = function (e) {
	e.preventDefault();
	$("#paymentmethod").addClass("cur");
	$("#coupons").removeClass("cur");
}
$(".cashdiv")[0].ontouchend = function (e) {
	e.preventDefault();
	$("#paymentmethod").removeClass("cur");
	$("#cash").addClass("cur");
	var orderids = $("#payment").data('orderid');
	var url = API_URL + "order.complete_pay&device_type=tablet&order_id=" + orderids + "&device_id=" + device_id +
		"&lang=" + DefaultLanguage;
	get_data(url);
}
$(".return3")[3].ontouchend = function (e) {
	e.preventDefault();
	$("#paymentmethod").addClass("cur");
	$("#cash").removeClass("cur");
}
$(".creditcarddiv")[0].ontouchend = function (e) {
	e.preventDefault();
	$("#paymentmethod").removeClass("cur");
	$("#creditcard").addClass("cur");
	var orderids = $("#payment").data('orderid');
	var url = API_URL + "order.complete_pay&device_type=tablet&order_id=" + orderids + "&device_id=" + device_id +
		"&lang=" + DefaultLanguage;
	get_data(url);
}
$(".return3")[4].ontouchend = function (e) {
	e.preventDefault();
	$("#paymentmethod").addClass("cur");
	$("#creditcard").removeClass("cur");
}
$(".roomfoliodiv")[0].ontouchend = function (e) {
	e.preventDefault();
	$("#paymentmethod").removeClass("cur");
	$("#roomfolio").addClass("cur");
	var orderids = $("#payment").data('orderid');
	var url = API_URL + "order.complete_pay&device_type=tablet&order_id=" + orderids + "&device_id=" + device_id +
		"&lang=" + DefaultLanguage;
	get_data(url);
}
$(".return3")[5].ontouchend = function (e) {
	e.preventDefault();
	$("#paymentmethod").addClass("cur");
	$("#roomfolio").removeClass("cur");
}

//点击购物车删除按钮事件
var delete_shangpin = function (targ) {
	$(targ).data("id")
	var url = API_URL + "cart.del&device_type=tablet&id=" + $(targ).data("id") + "&lang=" + DefaultLanguage;
	get_data(url).then(function (json) {
		if (json.ret == 200) {
			targ.parentNode.parentNode.removeChild(targ.parentNode);
			if ($(".prolist li").length == 0) {
				window.location.reload();
			}
		}
	});
}

//点击编辑按钮事件
$.each(writes, function (i) {
	writes[i].ontouchend = function (e) {
		e.preventDefault();
		if (writenow[i].className == "writenow cur") {
			writenow[i].className = "writenow";
			writecopy[i].className = "writecopy cur";
		} else {
			writenow[i].className = "writenow cur";
			writecopy[i].className = "writecopy";
		}
	}
});
//点击submit按钮事件
$.each(submitsub, function (j) {
	submitsub[j].ontouchend = function (e) {
		e.preventDefault();
		if (txtears[j].value == '') {
			writenow[j].className = "writenow";
			writecopy[j].className = "writecopy cur";
			writecopy[j].innerHTML = "&nbsp;";
		} else {
			writenow[j].className = "writenow";
			writecopy[j].className = "writecopy cur";
			writecopy[j].innerHTML = txtears[j].value;
		}
	}
});
